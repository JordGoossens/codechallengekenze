﻿using _6letterWords;
using _6letterWords.Tries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Trie6LetterWords
{
    internal class TreeProgram
    {
        private static void FillDictionaries()
        {
            try
            {
                using (var sr = new StreamReader("C:\\CodeChallenge\\codechallengekenze\\6letterWords\\input.txt"))
                {
                    string? line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Length == 6)
                        {
                            GlobalTries.SixLetterWords.Insert(line);
                        }
                        else
                        {
                            if (!GlobalTries.OtherWordParts.ContainsKey(line.Length))
                                GlobalTries.OtherWordParts[line.Length] = new Trie();
                            GlobalTries.OtherWordParts[line.Length].Insert(line);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }

        private static void FindMatchesRecursive(string word, List<string> parts, int remainingLength, int maxParts,
            HashSet<string> cache)
        {
            if (remainingLength == 0)
            {
                if (!GlobalTries.SixLetterWords.StartsWith(word)) return;

                var combinations = GlobalTries.SixLetterWords.GetCombinations(word);
                var combination = string.Join("+", parts);

                if (combinations.Any(c => c.combination == combination)) return;

                GlobalTries.SixLetterWords.AddCombination(word, combination, parts);
                return;
            }

            if (maxParts == 0) return;

            if (!string.IsNullOrEmpty(word) && !cache.Contains(word) &&
                !GlobalTries.SixLetterWords.StartsWith(word)) return;

            foreach (var key in GlobalTries.OtherWordParts.Keys)
            {
                foreach (var part in GlobalTries.OtherWordParts[key].GetAllWords())
                {
                    if (remainingLength - key >= 0)
                    {
                        var newWord = word + part;
                        cache.Add(newWord);
                        var newParts = new List<string>(parts) { part };
                        FindMatchesRecursive(newWord, newParts, remainingLength - key, maxParts - 1, cache);
                    }
                }
            }
        }

        private static void FindMatches()
        {
            ClearGlobalSixLetterWordTrieValues();
            var cache = new HashSet<string>();
            FindMatchesRecursive("", new List<string>(), 6, 4, cache);
        }

        private static void ClearGlobalSixLetterWordTrieValues()
        {
            GlobalTries.SixLetterWords.ClearAllCombinations();
        }

        private static void Main(string[] args)
        {
            FillDictionaries();
            FindMatches();
            GlobalTries.SixLetterWords.PrintAllWordsWithCombinations();
            Console.WriteLine("We made it through");
        }
    }
}
