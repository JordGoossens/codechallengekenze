﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _6letterWords
{
    public class TrieNode
    {
        public Dictionary<char, TrieNode> Children { get; } = new();
        public bool IsEndOfWord { get; set; }
    }

    public class Trie
    {
        private readonly TrieNode root;
        private readonly Dictionary<string, List<(string combination, List<string> parts)>> combinationsDictionary;
        private readonly HashSet<string> allWords;

        public Trie()
        {
            root = new TrieNode();
            combinationsDictionary = new Dictionary<string, List<(string combination, List<string> parts)>>();
            allWords = new HashSet<string>();
        }

        public void Insert(string word)
        {
            var currentNode = root;
            foreach (var letter in word)
            {
                if (!currentNode.Children.ContainsKey(letter))
                    currentNode.Children[letter] = new TrieNode();
                currentNode = currentNode.Children[letter];
            }
            currentNode.IsEndOfWord = true;
            allWords.Add(word);
        }

        public bool StartsWith(string prefix)
        {
            var currentNode = root;
            foreach (var letter in prefix)
            {
                if (!currentNode.Children.ContainsKey(letter))
                    return false;
                currentNode = currentNode.Children[letter];
            }
            return true;
        }

        public TrieNode GetNode(string word)
        {
            var currentNode = root;
            foreach (var letter in word)
            {
                if (!currentNode.Children.ContainsKey(letter))
                    return null;
                currentNode = currentNode.Children[letter];
            }
            return currentNode;
        }

        public void AddCombination(string word, string combination, List<string> parts)
        {
            if (!combinationsDictionary.ContainsKey(word))
            {
                combinationsDictionary[word] = new List<(string combination, List<string> parts)>();
            }

            if (!combinationsDictionary[word].Any(c => c.combination == combination))
            {
                combinationsDictionary[word].Add((combination, parts));
            }
        }

        public List<(string combination, List<string> parts)> GetCombinations(string word)
        {
            return combinationsDictionary.ContainsKey(word) ? combinationsDictionary[word] : new List<(string, List<string>)>();
        }

        public List<string> GetAllWords()
        {
            return allWords.ToList();
        }

        public void PrintAllWordsWithCombinations()
        {
            foreach (var word in combinationsDictionary.Keys)
            {
                Console.WriteLine($"Word: {word}");
                var wordCombinations = combinationsDictionary[word];
                if (wordCombinations.Count > 0)
                {
                    Console.WriteLine("Combinations:");
                    foreach (var (combination, parts) in wordCombinations)
                    {
                        Console.WriteLine($"  Combination: {combination}");
                        Console.WriteLine($"    Parts: {string.Join(" + ", parts)}");
                    }
                }
            }
        }
        public void ClearAllCombinations()
        {
            combinationsDictionary.Clear();
        }

    }
    
}
