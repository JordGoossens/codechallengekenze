﻿namespace _6letterWords;

internal class GlobalDictionaries
{
    public static Dictionary<string, ICollection<string>> SixLetterWords { get; } = new();

    //prevent unnecessary checks on lengths not adding up to 6 by storing the length as key
    public static Dictionary<int, ICollection<string>> OtherWordParts { get; } = new();

    private static void ClearGlobalSixLetterWordDictionaryValues()
    {
        foreach (var key in SixLetterWords.Keys) SixLetterWords[key].Clear();
    }

    internal static void PrintCombinationDictionary()
    {
        foreach (var key in SixLetterWords.Keys)
        {
            Console.WriteLine(key);
            foreach (var value in SixLetterWords[key]) Console.WriteLine(value);
        }
    }

    internal static void FillDictionaries(string inputFile)
    {
        ClearGlobalSixLetterWordDictionaryValues();
        try
        {
            var sr = new StreamReader(inputFile);

            string? line;
            while ((line = sr.ReadLine()) != null)
                if (line.Length == 6)
                    SixLetterWords.TryAdd(line, new List<string>());
                else if (OtherWordParts.ContainsKey(line.Length))
                    OtherWordParts[line.Length].Add(line);
                else
                    OtherWordParts.Add(line.Length, new List<string> { line });
        }
        catch (Exception e)
        {
            Console.WriteLine("The file could not be read:");
            Console.WriteLine(e.Message);
        }
    }

    internal static void FindMatches(int maxParts = 2)
    {
        try
        {
            ClearGlobalSixLetterWordDictionaryValues();
            //adding cache instead of startswith check
            var cache = new HashSet<string>();
            if (maxParts == 1) return;
            FindMatchesRecursive(string.Empty, new List<string>(), 6, cache, maxParts);
        }
        catch (Exception e)
        {
            Console.WriteLine("An error occurred while finding matches:");
            Console.WriteLine(e.Message);
        }
    }

    private static void FindMatchesRecursive(string word, List<string> parts, int remainingLength,
        HashSet<string> cache, int maxParts)
    {
        try
        {
            //base case
            if (remainingLength == 0)
            {
                //avoiding double lookup by using TryGetValue
                if (!SixLetterWords.TryGetValue(word, out var value)) return;
                var combination = string.Join("+", parts);
                if (value.Contains(combination)) return;
                value.Add(combination);

                return;
            }

            if (maxParts == 0) return;
            if (!word.Equals("") && !cache.Contains(word) &&
                SixLetterWords.Keys.Any(k => k.StartsWith(word))) return;
            foreach (var key in OtherWordParts.Keys)
            foreach (var part in OtherWordParts[key])
                if (remainingLength - key >= 0)
                {
                    var newWord = word + part;

                    cache.Add(newWord);
                    var newParts = new List<string>(parts) { part };
                    FindMatchesRecursive(newWord, newParts, remainingLength - key, cache, maxParts - 1);
                }
        }
        catch (Exception e)
        {
            Console.WriteLine("An error occurred while finding matches recursively:");
            Console.WriteLine(e.Message);
        }
    }
}