﻿namespace _6letterWords.Tries;

public static class GlobalTries
{
    public static readonly Trie SixLetterWords = new();
    public static readonly Dictionary<int, Trie> OtherWordParts = new();
}