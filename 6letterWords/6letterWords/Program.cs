﻿using _6letterWords;

internal class Program
{
    private static void Main(string[] args)
    {
        var command = "";
        while (command != "stop")
        {
            try
            {
                Console.WriteLine("Enter the maximum amount of parts:");

                var input = Console.ReadLine();

                var maxParts = string.IsNullOrEmpty(input) ? 2 : int.Parse(input);

                Console.WriteLine("Enter the absolute path to the input file:");
                Console.WriteLine(
                    "Press enter to use the default path: ../../../../input.txt");
                var inputFile = Console.ReadLine();

                if (string.IsNullOrEmpty(inputFile))
                    inputFile = "../../../../input.txt";

                GlobalDictionaries.FillDictionaries(inputFile);
                GlobalDictionaries.FindMatches(maxParts);
                
                GlobalDictionaries.PrintCombinationDictionary();
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                continue;
            }


            Console.WriteLine("Enter 'stop' to end the program or press any key to continue.");
            command = Console.ReadLine()?.ToLower();
        }
    }
}