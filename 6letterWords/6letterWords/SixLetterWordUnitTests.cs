﻿using System.Reflection;
using NUnit.Framework;
using NUnit.Framework.Legacy;
namespace _6letterWords;

internal class SixLetterWordUnitTests
{
    private const string TestFilePath = "../../../../testInput.txt";

    [SetUp]
    public void Setup()
    {
        GlobalDictionaries.FillDictionaries(TestFilePath);
    }

    [TestCase("../../../../testInput.txt", 22)]
    [TestCase("../../../../input.txt", 183)]
    public void TestFillDictionaries(string inputFile, int expectedDictionaryLength)
    {
        // Arrange
        GlobalDictionaries.FillDictionaries(inputFile);

        // Assert
        ClassicAssert.AreEqual(expectedDictionaryLength, GlobalDictionaries.SixLetterWords.Count);
        ClassicAssert.IsTrue(
            GlobalDictionaries.SixLetterWords.All(kvp => kvp.Key.Length == 6)); // All keys should be 6-letter words
        ClassicAssert.IsTrue(GlobalDictionaries.OtherWordParts.All(kvp => kvp.Key != 6)); // No keys should be 6
    }

    [TestCase(null)]
    [TestCase(1)]
    [TestCase(2)]
    //[TestCase(3)]
    public void TestFindMatches(int? maxParts)
    {
        var expectedWinkle = new List<List<string>>
        {
            new(),
            new() { "winkl+e", "wink+le", "wi+nkle", "win+kle", "w+inkle" }
            //new() {   },
        };
        var key = "winkle";


        if (maxParts.HasValue)
            GlobalDictionaries.FindMatches(maxParts.Value);
        else
            GlobalDictionaries.FindMatches();

        CollectionAssert.AreEqual(expectedWinkle[(int)(maxParts.HasValue ? maxParts - 1 : 1)],
            GlobalDictionaries.SixLetterWords[key]);
    }

    [Test]
    public void TestClearGlobalSixLetterWordDictionaryValues()
    {
        var methodInfo = typeof(GlobalDictionaries)
            .GetMethod("ClearGlobalSixLetterWordDictionaryValues", BindingFlags.NonPublic | BindingFlags.Static);
        methodInfo?.Invoke(null, null);

        // Assert all values are empty
        ClassicAssert.IsTrue(GlobalDictionaries.SixLetterWords.All(kvp => kvp.Value.Count == 0));
    }

    [Test]
    public void TestFillDictionaries_FileNotFound()
    {
        var inputFile = "non_existent_file.txt";

        using (var sw = new StringWriter())
        {
            Console.SetOut(sw);
            GlobalDictionaries.FillDictionaries(inputFile);
            var result = sw.ToString().Trim();
            ClassicAssert.IsTrue(result.Contains("The file could not be read:"));
        }
    }
    
}